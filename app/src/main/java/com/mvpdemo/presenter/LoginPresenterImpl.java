package com.mvpdemo.presenter;

import com.mvpdemo.api.ApiClient;
import com.mvpdemo.api.ApiInterface;
import com.mvpdemo.model.GenericResponse;
import com.mvpdemo.view.interfaces.LoginView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by priyanka on 8/2/18.
 * Implementation class of Login Interface
 */

public class LoginPresenterImpl implements LoginPresenter {

    LoginView mView;
    private ApiInterface apiInterface;


    public LoginPresenterImpl(LoginView view) {
        this.mView=view;        //assigning LoginView to this view

    }

    @Override
    public void onLogin(String email, String pwd) {
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        apiInterface.getLoginResponse().enqueue(new Callback<GenericResponse>() {
            @Override
            public void onResponse(Call<GenericResponse> call, Response<GenericResponse> response) {
                mView.onSuccess(response);            //send response if you want to send model class data to your activity
            }

            @Override
            public void onFailure(Call<GenericResponse> call, Throwable t) {

            }
        });
    }
}
