package com.mvpdemo.presenter;

/**
 * Created by priyanka on 8/2/18.
 */

public interface LoginPresenter {

    void onLogin(String email,String pwd);
}
