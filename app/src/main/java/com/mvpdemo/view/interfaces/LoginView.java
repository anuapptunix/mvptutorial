package com.mvpdemo.view.interfaces;

import com.mvpdemo.model.GenericResponse;

import retrofit2.Response;

/**
 * Created by priyanka on 8/2/18.
 */

public interface LoginView {

    void onValidate();

    void onSuccess(Response<GenericResponse> s);
}
