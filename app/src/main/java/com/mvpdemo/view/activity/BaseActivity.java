package com.mvpdemo.view.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by priyanka on 7/2/18.
 */

public class BaseActivity extends AppCompatActivity {

    ProgressDialog mProgressDialog;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState, @Nullable PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
    }


    /**
     * Method to show progress dialog
     * @param msg
     */
    public void showProgress(String msg){
        if(mProgressDialog!=null && mProgressDialog.isShowing()){
            mProgressDialog.dismiss();
        }
        mProgressDialog= new ProgressDialog(this);
        mProgressDialog.setMessage(msg);
        mProgressDialog.show();
    }


    /**
     * Method for Hiding progress dialog
     */
    public void dismissProgress(){
        if(mProgressDialog!=null){
            mProgressDialog.dismiss();
            mProgressDialog=null;
        }
    }


}


