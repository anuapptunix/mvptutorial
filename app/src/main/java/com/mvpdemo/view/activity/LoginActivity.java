package com.mvpdemo.view.activity;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.mvpdemo.R;
import com.mvpdemo.model.GenericResponse;
import com.mvpdemo.presenter.LoginPresenter;
import com.mvpdemo.presenter.LoginPresenterImpl;
import com.mvpdemo.view.interfaces.LoginView;

import retrofit2.Response;

/**
 * Created by priyanka on 7/2/18.
 */

public class LoginActivity extends BaseActivity implements LoginView {

        LoginPresenter mPresenter;
        EditText et_email;
        EditText et_pwd;
    Button btn_login;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        et_email= (EditText) findViewById(R.id.et_email);
        et_pwd= (EditText) findViewById(R.id.et_pwd);
        btn_login=(Button) findViewById(R.id.btn_login);
        mPresenter=new LoginPresenterImpl(this);
        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPresenter.onLogin(et_email.getText().toString(),et_pwd.getText().toString());
            }
        });
    }
//jhsdfjkhasdkjfjasdkfuyhasdjhfdhcgdashh

    @Override
    public void onValidate() {
        //Validate email and other inputs

    }



    @Override
    public void onSuccess(Response<GenericResponse> s) {

        Toast.makeText(this,"Congrats for your first MVP "+s.body().getTitle().toString(),Toast.LENGTH_LONG).show();
    }
}
